function initToggleTables(selector) {
	const containers = document.querySelectorAll(selector);

	for (let i = 0; i < containers.length; i++) {
		const table = containers[i].querySelector('table');
		const cells = table.querySelectorAll('td');

		checkSizeTable(table, containers[i]);

		for (let e = 0; e < cells.length; e++) {
			const cell = cells[e];

			cell.addEventListener('click', function(e) {
				clickOnCell(e.target, table);
			});
		}
	}
}

function checkSizeTable(table, container) {
	const containerWidth = container.offsetWidth;
	const tableWidth = table.offsetWidth;
	const rows = table.querySelectorAll('tr');

	let tableAside = document.createElement('table');
	tableAside.classList.add('customTable', 'customTable_aside');
	tableAside.innerHTML = '<tbody></tbody>';

	let tableAsideTbody = tableAside.querySelector('tbody');

	if (tableWidth <= containerWidth) return false;

	for (let r = 0; r < rows.length; r++) {
		const cell = rows[r].querySelector('td, th');
		if (cell) {
			let tr = document.createElement('tr');
			tr.append(cell.cloneNode(true));
			tableAsideTbody.append(tr);
		}
	}

	container.append(tableAside);
}

function clickOnCell(cell, table) {
	const horizont = cell.getAttribute('data-horizont-id');
	const vertical = cell.getAttribute('data-vertical-id');
	const url = table.getAttribute('data-url');
	const id = table.getAttribute('data-id');

	let data = new FormData();
	data.append('horizont-id', horizont);
	data.append('vertical-id', vertical);
	data.append('table-id', id);

	if (horizont && vertical && url) {
		$.ajax({
			url : url,
			type : 'POST',
			data: data,
			cache: false,
			contentType: false,
			processData: false,
			success : function(data) {
				// if (data.result == 'success') {}
				toggleViewCell(cell);
			},
			error: function (data) {
				console.error(data);
			}
		});
	}
}

function toggleViewCell(cell) {
	const view = cell.getAttribute('data-view');
	if (view === 'on') {
		cell.setAttribute('data-view', 'off');
	} else {
		cell.setAttribute('data-view', 'on');
	}
}

document.addEventListener("DOMContentLoaded", () => {
	initToggleTables('.js-toggle-table-container');
});