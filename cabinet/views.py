from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import LoginView, LogoutView
from django.shortcuts import render, resolve_url
from django_site import settings


class RcLoginView(LoginView):
    template_name = 'profile/login.html'

    def get_success_url(self):
        return resolve_url(settings.LOGIN_REDIRECT_URL)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Вход пользователя'
        context['description'] = 'Вход пользователя'
        return context


@login_required(login_url='/')
def profile(request):
    context = {
        'title': 'Профиль пользователя',
        'description': 'Профиль пользователя',
    }
    return render(request, 'profile/profile.html', context)


class RcLogoutView(LoginRequiredMixin, LogoutView):
    template_name = 'profile/logout.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Выход пользователя'
        context['description'] = 'Выход пользователя'
        return context

