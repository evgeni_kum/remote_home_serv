jQuery(document).ready(function($){
    show_progress();
    //setInterval('show_progress()',1000);   // ms
});

var job_load = 1;

function show_progress()
        {
            var url = $("#ProgressBlock").attr("data-progress-url");  // get the url in "progress"
            //var pgrbar = $('#progress-bar')
            var url = $("#ProgressBlock").attr("data-progress-url");

            var pgrbar_new = $('#progress-bar-new')

            $.ajax({
                url: url,
                cache: false,
                success: function (data) {
                    var current_lev = data.progress.percent;
                    //pgrbar.attr('value', current_lev);
                    pgrbar_new.attr('aria-valuenow', current_lev);
                    pgrbar_new.attr('style', 'width: ' + current_lev + '%');

                    $('.toast').toast('show')

                    if (!data.complete) {
                        $('#messages_task').html('Выполнено: ' + current_lev + '%');
                        setTimeout(function() {
                            show_progress();
                        }, 1000);
                    }
                    else{
                        $('#messages_task').html('Готово!');
                    }
                },
                error: function (data) {
                    $('#messages_task').html("Ошибка!");
                }
            });
        }
;

