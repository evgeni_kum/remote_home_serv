axios.defaults.xsrfCookieName = 'csrftoken';
axios.defaults.xsrfHeaderName = "X-CSRFTOKEN";

const app = Vue.createApp({
    data() {
        return {
            modeWide: false,
            activeClass: 'col-md-6',
            activeAddColClass: 'col-md-6',
            clockClass: 'digit-clock digit-clock-big',
            DateClockClass: 'bigDateClockClass',
            all_photo: [],
            data_photo_url: '',
            active_num_photo: 0,
            count_photo: 0,
            active_photo: '',
            isActive: false,
            templateName: '',
            timeoutID: null,
            showButtonMode: false,
            blockDateTimeClass: 'defaultDateTimeClass',
            blockButtonsClass: 'defaultBlockButtons',
        }
    },
    delimiters: ['{*', '*}'],
    // for standalone
    compilerOptions: {
        delimiters: ['{*', '*}']
    },
    created: function () {
        //this.reloadAllPhoto();
    },
    mounted(){
        this.templateName = document.getElementById('app').getAttribute('data-template');
        this.data_photo_url = document.getElementById('clock').getAttribute('data-photo-url');
        this.reloadAllPhoto();
        this.changeModePage();
    },
    computed:{},
    methods: {
        clearAllVariable: function () {
        },
        changeModePage: function (event) {
            if (this.templateName == 'photo_border'){
                // у рамки без графика нет переключателя ширины
                this.activeClass = 'col-lg-1 col-md-1 col-sm-2 col-xs-2';
                this.activeAddColClass = 'col-lg-11 col-md-11 col-sm-10 col-xs-10';
                this.clockClass = 'digit-clock digit-clock-big';
                this.DateClockClass = 'bigDateClockClass';
            } else {
                if (this.modeWide){
                    this.modeWide = false;
                    this.activeClass = 'col-md-6';
                    this.activeAddColClass = 'col-md-6';
                    this.clockClass = 'digit-clock digit-clock-big';
                    this.DateClockClass = 'bigDateClockClass';
                } else {
                    this.modeWide = true;
                    this.activeClass = 'col-md-8';
                    this.activeAddColClass = 'col-md-4';
                    this.clockClass = 'digit-clock digit-clock-small';
                    this.DateClockClass = 'smallDateClockClass'
                }
            }

        },
        reloadAllPhoto: function (event) {
            clearTimeout(this.timeoutID);
            axios
              .get(this.data_photo_url)
              .then(response => {
                this.all_photo = response.data.all_photo;
                this.count_photo = this.all_photo.length;
                this.active_photo = this.all_photo[this.active_num_photo].img;
                this.active_num_photo = this.all_photo.length - 1;
                this.switchPhoto();
                console.log('reload photo is complete');
              })
              .catch(error => {
                console.log(error);
              });
        },
        switchPhoto: function (event) {
            this.all_photo[this.active_num_photo].class = 'photo'
            this.active_num_photo = this.active_num_photo + 1;
            if (this.active_num_photo == this.count_photo){
                this.active_num_photo = 0;
            }
            //this.active_photo = this.all_photo[this.active_num_photo].img;

            //console.log(this.active_num_photo);
            this.all_photo[this.active_num_photo].class = 'active-photo'

            this.timeoutID = setTimeout(() => {
                this.switchPhoto();
              }, 10000);
        },
        switchPhotoToLeft: function (event) {
            this.all_photo[this.active_num_photo].class = 'photo'
            this.active_num_photo = this.active_num_photo - 1;
            if (this.active_num_photo < 0){
                this.active_num_photo = this.count_photo - 1;
            }

            //console.log(this.active_num_photo);
            this.all_photo[this.active_num_photo].class = 'active-photo'
            clearTimeout(this.timeoutID);
            this.timeoutID = setTimeout(() => {
                this.switchPhoto();
            }, 10000);
        },
        switchPhotoToRight: function (event) {
            this.all_photo[this.active_num_photo].class = 'photo'
            this.active_num_photo = this.active_num_photo + 1;
            if (this.active_num_photo == this.count_photo){
                this.active_num_photo = 0;
            }

            //console.log(this.active_num_photo);
            this.all_photo[this.active_num_photo].class = 'active-photo'
            clearTimeout(this.timeoutID);
            this.timeoutID = setTimeout(() => {
                this.switchPhoto();
            }, 10000);
        },
        switchToShowButtonMode: function (event) {
            if (!this.showButtonMode){
                this.showButtonMode = true;
                this.blockDateTimeClass = 'disableDateTimeClass';
                this.blockButtonsClass = 'activeBlockButtons';
                // console.log(this.showButtonMode);
            }
        },
        switchToHideButtonMode: function (event) {
            if (this.showButtonMode){
                this.showButtonMode = false;
                this.blockDateTimeClass = 'defaultDateTimeClass';
                this.blockButtonsClass = 'defaultBlockButtons';
                // console.log(this.showButtonMode);
            }
        }
    }
});
// Mount the app to the DOM
app.mount('#app');