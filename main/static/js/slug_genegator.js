let form = document.forms.inputName;

form.name.onchange = getSlug;
form.name.oninput = getSlug;

function getSlug(){
    var name_value = $("#id_name").val();
    console.log(name_value);

    var url = $("#input-form").attr("data-url");
    console.log(url);
    $.ajax({                       // initialize an AJAX request
        url: url,                    //
        data: {
          '_string': name_value       // add the mark to the GET parameters
        },
        success: function (data) {   // `data` is the return of the `load_cities` view function
            $('#id_slug').val(data.result);   // передаем текст в поле ввода slug
        }
      });
};