import datetime
import random
from django.db.models import Avg, Subquery, OuterRef
from django.utils import timezone
from rest_framework import status
from rest_framework.authentication import TokenAuthentication, SessionAuthentication
from rest_framework.decorators import api_view, permission_classes, authentication_classes
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.views import APIView
from rest_framework.response import Response
from api.serializers import EntrySerializer, PhotoSerializer, SetupEntrySerializer
from logs.server_log_config import app_info
from main.models import GlobalSettings
from photogallery.models import Photo
from serv_req.models import Entry, Sensor, EntryInBuffer, Device, SetupEntry
from serv_req.services import toFixed, calc_local_date


"""
@api_view(('GET',))
@authentication_classes([SessionAuthentication, BasicAuthentication, TokenAuthentication])
@permission_classes([IsAuthenticated, IsAdminUser])
def manager_service_request(request, t_nictame):
    # получаем все service_request (заявки) менеджера
    serv_req = ServiceRequests.objects.filter(manager__t_nicname=t_nictame)
    serializer = ServiceRequestsSerializer(serv_req, many=True)
    res = {
        'error': False,
        'serv_req': serializer.data
    }
    return Response(res, status.HTTP_200_OK)
"""


class EntryView(APIView):
    """ Точка входа данных """
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated, IsAdminUser]

    def post(self, request):
        data = request.data

        # Очистка записей старше 30 дней
        not_older = timezone.now() - datetime.timedelta(days=30)
        Entry.objects.filter(created__lt=not_older).delete()

        # создать запись
        serializer = EntrySerializer(data=data)  # если json
        if serializer.is_valid():
            new_entry = serializer.save()
            new_entry.owner = request.user
            new_entry.save()

            res = {
                'error': False,
                'data': serializer.data
            }
            return Response(res, status.HTTP_200_OK)
        else:
            return Response({"errors": serializer.errors}, status.HTTP_400_BAD_REQUEST)


class EntryArrayView(APIView):
    """ Точка входа массива данных """
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated, IsAdminUser]

    def post(self, request):
        data = request.data
        # print(f'{data=}')
        array = data.get('array')

        # Очистка записей старше 30 дней
        not_older = timezone.now() - datetime.timedelta(days=30)
        Entry.objects.filter(created__lt=not_older).delete()

        result = []
        error = False

        for item_data in array:
            sensor_uid = item_data.get('sensor')

            try:
                sensor = Sensor.objects.filter(uid=sensor_uid).first()
            except Exception as e:
                error = True
                print(str(e))
                result.append({
                    'error': True,
                    'sensor': 'Error',
                    'data': str(e)
                })
                continue
            if sensor:
                if Entry.objects.filter(sensor=sensor):
                    # проверку делаем если записи до этого были
                    entry = Entry.objects.filter(sensor=sensor).first()

                    # дифференцированно выбираем задержку между записями в БД, для каждого сенсора
                    # current_delay = entry.created + datetime.timedelta(minutes=entry.sensor.delay)
                    # now = timezone.now()
                    # now = timezone.make_aware(datetime.datetime.now())
                    # print(f'{current_delay=}')
                    # print(f'{now=}')
                    if entry.created + datetime.timedelta(minutes=entry.sensor.delay) > timezone.now():
                        # положенное время еще не прошло, запись запрещена
                        error = True
                        result.append({
                            'error': False,
                            'sensor': item_data.get('sensor'),
                            'data': "Положенное время еще не прошло, запись запрещена"
                        })
                    else:
                        # положенное время уже прошло, запись разрешена
                        buffer_entrys = EntryInBuffer.objects.filter(sensor=sensor)
                        if not buffer_entrys:
                            # если нет записей в буффере, то вносим запись
                            # item_data = {'sensor': '5b195ed6-22cb-4540-bb46-dd59118fc83e', 'value': '25.6', 'owner': 'home_bot_01'}
                            EntryInBuffer.objects.create(sensor=sensor, value=item_data.get('value'))
                            error = False
                            result.append({
                                'error': False,
                                'sensor': f'{sensor.name}, uid={sensor.uid}',
                                'data': f"Создали в буффере запись 0"
                            })
                        else:
                            last_entry = buffer_entrys.first()
                            if last_entry.count_value != 4:
                                count_value = last_entry.count_value + 1
                                EntryInBuffer.objects.create(sensor=sensor, value=item_data.get('value'), count_value=count_value)
                                error = False
                                result.append({
                                    'error': False,
                                    'sensor': f'{sensor.name}, uid={sensor.uid}',
                                    'data': f"Создали в буффере запись {count_value}"
                                })
                            else:
                                # буффер заполнен
                                entrys_avg = EntryInBuffer.objects.aggregate(Avg('value'))

                                entrys_avg_value = entrys_avg.get('value__avg')     # среднее значение
                                item_data['value'] = toFixed(entrys_avg_value, 2)
                                # print(f'{entrys_avg=}, -> {toFixed(entrys_avg_value, 2)}')
                                buffer_entrys.delete()

                                serializer = EntrySerializer(data=item_data)  # если json
                                if serializer.is_valid():
                                    new_entry = serializer.save()
                                    new_entry.owner = request.user
                                    new_entry.save()

                                    result.append({
                                        'error': False,
                                        'sensor': f'{sensor.name}, uid={sensor.uid}',
                                        'data': serializer.data
                                    })
                                else:
                                    error = True
                                    result.append({
                                        'error': True,
                                        'sensor': f'{sensor.name}, uid={sensor.uid}',
                                        'data': serializer.errors
                                    })

                else:
                    # создать запись
                    serializer = EntrySerializer(data=item_data)  # если json

                    if serializer.is_valid():
                        new_entry = serializer.save()
                        new_entry.owner = request.user
                        new_entry.save()

                        result.append({
                            'error': False,
                            'sensor': f'{sensor.name}, uid={sensor.uid}',
                            'data': serializer.data
                        })
                    else:
                        error = True
                        result.append({
                            'error': True,
                            'sensor': f'{sensor.name}, uid={sensor.uid}',
                            'data': serializer.errors
                        })
            else:
                error = True
                result.append({
                    'error': True,
                    'sensor': item_data.get('sensor'),
                    'data': "Датчик не найден"
                })

        res = {
            'error': error,
            'result': result
        }
        return Response(res, status.HTTP_200_OK)


class EntryTestView(APIView):
    """ Точка входа данных """
    authentication_classes = [TokenAuthentication, SessionAuthentication]
    permission_classes = [IsAuthenticated, IsAdminUser]

    def post(self, request):
        data = request.data
        # print(data)

        # Очистка записей старше 30 дней
        not_older = timezone.now() - datetime.timedelta(days=30)
        Entry.objects.filter(created__lt=not_older).delete()

        local_datetime, current_datetime = calc_local_date()  # текущий мес - год,

        res = {
            'error': False,
            'local_datetime': local_datetime,
            'description': 'Success'
        }
        entry_sensor = Entry.objects.filter(sensor__external=True).first()
        if entry_sensor:
            res['outside'] = toFixed(entry_sensor.value, 1)
        else:
            res['outside'] = ''

        entry_ex_sensor = Entry.objects.filter(sensor__internal=True).first()
        if entry_ex_sensor:
            res['inside'] = toFixed(entry_ex_sensor.value, 1)
        else:
            res['inside'] = ''

        sensors = data.get('sensors')
        result = []
        for i, item_sensor in enumerate(sensors):
            sensor_uid = item_sensor.get('sensor')
            sensor = Sensor.objects.filter(uid=sensor_uid).first()
            if not sensor:
                res['error'] = True
                result.append({"sensor": sensor_uid, "description": f'sensor with uid={sensor_uid}, not found'})
                app_info(f'{i=} {sensor_uid=}, sensor not found')
                continue
            entry = Entry.objects.filter(sensor=sensor).first()
            if entry:
                if entry.created + datetime.timedelta(minutes=entry.sensor.delay) > timezone.now():
                    # положенное время еще не прошло, запись запрещена
                    result.append({"sensor": sensor_uid, "description": "Положенное время еще не прошло, запись запрещена"})
                    app_info(f'{i=} {sensor_uid=}, положенное время еще не прошло, запись запрещена')
                    continue

            # положенное время уже прошло, запись разрешена
            if sensor.avg_value:
                buffer_entrys = EntryInBuffer.objects.filter(sensor=sensor)
                value_sens = item_sensor.get('value')
                if not buffer_entrys:
                    # если нет записей в буффере, то вносим запись
                    # item_data = {'sensor': '5b195ed6-22cb-4540-b59118fc83e', 'value': '25.6', 'owner': 'home_bot_01'}
                    EntryInBuffer.objects.create(sensor=sensor, value=value_sens)
                    mess = f"{i=}, Создали в буффере запись 0, {value_sens}"
                    result.append({"sensor": sensor_uid, "description": mess})
                    app_info(mess)
                    continue
                else:
                    last_entry = buffer_entrys.first()
                    if last_entry.count_value < 3:
                        count_value = last_entry.count_value + 1

                        EntryInBuffer.objects.create(sensor=sensor, value=value_sens,
                                                     count_value=count_value)
                        mess = f"{i=}, Создали в буффере запись {count_value}, {value_sens}"
                        result.append({"sensor": sensor_uid, "description": mess})
                        app_info(mess)
                        continue
                    else:
                        # буфер заполнен
                        EntryInBuffer.objects.create(sensor=sensor, value=value_sens, count_value=4)
                        entrys_in_buffer = EntryInBuffer.objects.filter(sensor=sensor)
                        entrys_avg = entrys_in_buffer.aggregate(Avg('value'))
                        entrys_avg_value = entrys_avg.get('value__avg')  # среднее значение
                        item_sensor['value'] = toFixed(entrys_avg_value, 2)  # среднее значение
                        buffer_entrys.delete()
                        # буфер заполнен, можно создавать запись
                        value = item_sensor.get('value')
                        Entry.objects.create(sensor=sensor, value=value, owner=request.user)  # если json
                        mess = f'{i=}, Сохранено среднее значение:{value}, последнее {value_sens}'
                        result.append({"sensor": sensor_uid,
                                       "description": mess})
                        app_info(mess)
                        continue

            value = item_sensor.get('value')
            Entry.objects.create(sensor=sensor, value=value, owner=request.user)  # если json
            result.append({"sensor": sensor_uid, "description": f"Запись данных сохранена: {value}"})
            app_info(f'{i=} {sensor_uid=}, запись данных сохранена: {value}')

        res['result'] = result
        return Response(res, status.HTTP_200_OK)

    def get(self, request):
        data = request.data
        print(data)

        sub_query = Subquery(Entry.objects.filter(sensor=OuterRef('pk')).order_by('-created').values('value')[:1])
        all_device = Sensor.objects.annotate(last_entry=sub_query).filter(active=True).order_by('rating')
        local_datetime, current_datetime = calc_local_date()  # текущий мес - год,
        sensor = all_device.first()
        entry_sensor = Entry.objects.filter(sensor=sensor).order_by('-created').first()

        res = {
            'error': False,
            'data': {
               'local_datetime': local_datetime,
               'outside': entry_sensor.value
            }
        }
        return Response(res, status.HTTP_200_OK)


@api_view(('GET',))
@authentication_classes([SessionAuthentication, TokenAuthentication])
@permission_classes([IsAuthenticated,])
def entries_data(request):
    uids = request.GET.get('uid')
    datetime_now = timezone.localtime(timezone.now())
    now = datetime_now.strftime('%Y-%m-%d %H:%M:%S')
    year = datetime_now.strftime('%Y')
    month = datetime_now.strftime('%m')
    day = datetime_now.strftime('%d')
    hour = datetime_now.strftime('%H')
    minute = datetime_now.strftime('%M')
    second = datetime_now.strftime('%S')
    if not uids:
        res = {
            'error': False,
            'datetime_now': now,
            'description': 'Не указан uid',
            'year': year,
            'month': month,
            'day': day,
            'hour': hour,
            'minute': minute,
            'second': second,
        }
        return Response(res, status.HTTP_200_OK)
    items_uid = uids.split(',')
    result = []
    for item_id in items_uid:
        entry = Entry.objects.filter(sensor__uid=item_id).first()
        if entry:
            value = toFixed(entry.value, 1)
            result.append({
                "uid": item_id,
                "value": value
            })
    # print(result)
    res = {
        'error': False,
        'datetime_now': now,
        'values': result,
        'year': year,
        'month': month,
        'day': day,
        'hour': hour,
        'minute': minute,
        'second': second,
    }
    return Response(res, status.HTTP_200_OK)


@api_view(('GET',))
@authentication_classes([SessionAuthentication,])
@permission_classes([IsAuthenticated,])
def get_photo(request):
    # global_settings = GlobalSettings.objects.all().first()

    all_photo = Photo.objects.all().order_by('section')
    serializer = PhotoSerializer(all_photo, many=True)

    # print(result)
    res = {
        'error': False,
        # 'photo': photo.img.url,
        'all_photo': serializer.data
    }
    return Response(res, status.HTTP_200_OK)


@api_view(('GET',))
@authentication_classes([SessionAuthentication, TokenAuthentication])
@permission_classes([IsAuthenticated,])
def settings_data(request):
    uid = request.GET.get('uid')
    if not uid:
        res = {
            'error': False,
            'description': 'Не указан uid',
        }
        return Response(res, status.HTTP_200_OK)

    device = Device.objects.filter(uid=uid).first()
    if not device:
        res = {
            'error': False,
            'description': 'Устройство не найдено',
        }
        return Response(res, status.HTTP_200_OK)

    setup_entry = SetupEntry.objects.filter(device=device).order_by('rating')
    serializer = SetupEntrySerializer(setup_entry, many=True)
    data = serializer.data if setup_entry else []
    res = {'error': False,
           'uid':uid
           }
    for item in data:
        res[item['uid']] = item['value']

    return Response(res, status.HTTP_200_OK)
