import datetime
import os
import re

if __name__ == '__main__':
    my_dir_list = os.listdir()
    for item_file in my_dir_list:
        if item_file == "local_settings.py":
            with open("local_settings.py", 'r') as f:
                file_dump = f.read()

            patt = re.compile('SCRIPT_VERSION[\s][=][\s][\"][\d]+[\"]', re.S)
            find = patt.findall(file_dump)
            if find:
                print(f'Нашли {find}')
                now = datetime.datetime.now()
                dtm = now.strftime('%d%m%y%H%M%S')
                print(f'Обновляем на {dtm}')
                sub_text = patt.sub(f'SCRIPT_VERSION = "{dtm}"', file_dump)
                with open("local_settings.py", 'w') as f:
                    f.write(sub_text)

            else:
                print('Не нашли строку SCRIPT_VERSION')
