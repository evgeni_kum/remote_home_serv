from django.urls import path

from main.views import photo_border, photo_border_ext

app_name = 'main_app'

urlpatterns = [
    path('photo-border/', photo_border, name='photo_border'),
    path('photo-border-ext/', photo_border_ext, name='photo_border_ext'),
]

