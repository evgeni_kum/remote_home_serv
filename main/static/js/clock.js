var items = [];
const initAllData = () => {
    const elements = document.querySelectorAll(".entry-value");
    if (!elements[0]) return false;

    for (var i = 0; i < elements.length; i++) {
        var uid = elements[i].getAttribute('data-id');
        //console.log(uid);
        items.push(uid);
    };
}

/*global window*/
    (function (global) {
        "use strict";
        function Clock(el) {
            var document = global.document;
            this.el = document.getElementById(el);
            this.months = ['Января', 'Февраля', 'Марта', 'Апреля', 'Мая', 'Июня', 'Июля', 'Августа', 'Сентября', 'Октября', 'Ноября', 'Декабря'];
            this.days = ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'];
        }
        Clock.prototype.addZero = function (i) {
            if (i < 10) {
                i = "0" + i;
                return i;
            }
            return i;
        };
        Clock.prototype.updateClock = function () {
            var now, year, month, dayNo, day, hour, minute, second, result, self;
            now = new global.Date();
            year = now.getFullYear();
            month = now.getMonth();
            dayNo = now.getDay();
            day = now.getDate();
            hour = this.addZero(now.getHours());
            minute = this.addZero(now.getMinutes());
            second = this.addZero(now.getSeconds());
            //result = this.days[dayNo] + ", " + day + " " + this.months[month] + " " + year + " " + hour + ":" + minute + ":" + second;
            result = hour + ":" + minute;
            var result_date =  this.days[dayNo] + ", " + day + " " + this.months[month];
            self = this;
            self.el.innerHTML = result;

            var date_clock = document.getElementById('dateClock')
            dateClock.innerHTML = result_date;

            var data_url = document.getElementById('clock').getAttribute('data-url');
            var url = data_url + '?uid=' + items;
            // console.log(url);

            $.getJSON(url, function (result) {
                //$('.captcha').attr('src', result['image_url']);
                //$('#id_captcha_0').val(result['key'])
                for (var i = 0; i < result.values.length; i++) {
                    //console.log(result.values[i]);
                    const elements = document.querySelectorAll(".entry-value");
                    for (var i = 0; i < elements.length; i++) {
                        var item_uid = elements[i].getAttribute('data-id');
                        if (item_uid === result.values[i].uid){
                            elements[i].textContent = result.values[i].value;
                        }
                    };

                };

            });

            global.setTimeout(function () {
                self.updateClock();
            }, 10000);
        };
        global.Clock = Clock;
    }(window));

    function addEvent(elm, evType, fn, useCapture) {
        "use strict";
        if (elm.addEventListener) {
            elm.addEventListener(evType, fn, useCapture);
        } else if (elm.attachEvent) {
            elm.attachEvent('on' + evType, fn);
        } else {
            elm['on' + evType] = fn;
        }
    }

    addEvent(window, "load", function () {
        if (document.getElementById("clock")) {
            var clock = new Clock("clock");
            clock.updateClock();
        }
    });

document.addEventListener("DOMContentLoaded", () => {
    initAllData();
});