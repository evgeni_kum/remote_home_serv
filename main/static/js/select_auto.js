jQuery(document).ready(function($){
    $("#change-mark").change(function () {
      var url = $("#selectAutoForm").attr("data-model-url");  // get the url of the `mark` view
      var markId = $(this).val();  // get the selected country ID from the HTML input
      // console.log(url);
      $.ajax({                       // initialize an AJAX request
        url: url,                    //
        data: {
          'mark': markId       // add the mark to the GET parameters
        },
        success: function (data) {   // `data` is the return of the `load_cities` view function
          $("#change-model").html(data);  // replace the contents of the mark input with the data that came from the server
          $('#change-model').prop('disabled', false);
          $('#change-year').prop('disabled', true);
          $('#id-go-car').prop('disabled', true);
        }
      });
    });
});
jQuery(document).ready(function($){
    $("#change-model").change(function () {
      var url = $("#selectAutoForm").attr("data-year-url");
      var modelId = $("#change-model").val();
      $.ajax({                       // initialize an AJAX request
        url: url,                    //
        data: {
          'model': modelId
        },
        success: function (data) {
          $("#change-year").html(data);
          $('#change-year').prop('disabled', false);
          $('#id-go-car').prop('disabled', true);
        }
      });
    });
});
jQuery(document).ready(function($){
    $("#change-year").change(function () {
      var url = $("#selectAutoForm").attr("data-year-url");
      $('#id-go-car').prop('disabled', false);
    });
});
document.getElementById('id-go-car').onclick = function() {
    var base_url = $("#selectAutoForm").attr("data-base-url");
    var mark = $('select[name=change-mark]').val();
    var model = $('select[name=change-model]').val();
    var year = $('select[name=change-year]').val();
    var url = base_url  + model + '/' + year + '/';
    console.log(url);
    document.location.href = url;
};

