from django.contrib import admin

from main.models import GlobalSettings


class GlobalSettingsAdmin(admin.ModelAdmin):
    list_display = ('name',)


admin.site.register(GlobalSettings, GlobalSettingsAdmin)
