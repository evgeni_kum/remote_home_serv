from django.db import models
from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver
from django.utils import timezone
from django.contrib import admin
from cabinet.models import AdvUser


class WidthEntry(models.Model):
    # Запись данных про вес
    created = models.DateTimeField(default=timezone.now)
    value = models.DecimalField(max_digits=8, decimal_places=2, verbose_name="Значение")
    owner = models.ForeignKey(AdvUser, on_delete=models.SET_NULL, null=True, blank=True,
                              verbose_name='Чья запись')

    TYPE_ENTRY = (
        ('width', "Вес"),
        ('waist', "Талия"),
    )
    type = models.CharField(max_length=10, choices=TYPE_ENTRY, default='width', verbose_name="Тип записи")

    def pyre_dadetime(self):
        current_tz = timezone.get_current_timezone()
        local = self.created.astimezone(current_tz)
        return local.strftime("%H:%M %d/%m/%Y")

    class Meta:
        verbose_name_plural = 'Записи данных'
        verbose_name = 'Запись данных'
        ordering = ['-created']

    def __str__(self):
        return f'{self.value}'


@admin.register(WidthEntry)
class AdminWidthEntry(admin.ModelAdmin):
    list_display = ('created', 'value', 'type', 'owner')


class EventEntry(models.Model):
    # Событие
    created = models.DateTimeField(default=timezone.now)
    value = models.DecimalField(max_digits=8, decimal_places=2, verbose_name="Значение")
    owner = models.ForeignKey(AdvUser, on_delete=models.SET_NULL, null=True, blank=True,
                              verbose_name='Чья запись')
    T_IVENT = (
        ('none', 'не определено'),
        ('trenirovka', 'тренировка'),
    )

    type_event = models.CharField(max_length=20, choices=T_IVENT)

    class Meta:
        verbose_name_plural = 'События'
        verbose_name = 'Событие'
        ordering = ['-created']

    def __str__(self):
        return f'{self.value}'


@admin.register(EventEntry)
class AdminEventEntry(admin.ModelAdmin):
    list_display = ('created', 'type_event', 'value', 'owner')


class GlobalSettings(models.Model):
    name = models.CharField(max_length=100, verbose_name='Название', default='Глобальные настройки')
    active_section = models.PositiveIntegerField(verbose_name='Активная категория фотоальбома', default=0)

    def __str__(self):
        return self.name


"""
@admin.register(GlobalSettings)
class AdminGlobalSettings(admin.ModelAdmin):
    list_display = ('name', 'pk', 'loading_products', 'viber_whatsapp', 'telegram', 'delivery')
"""


@receiver(post_save, sender=GlobalSettings)
def create_(sender, instance, created, **kwargs):
    all_set = GlobalSettings.objects.all()
    last_obj = GlobalSettings.objects.all().first()
    for item in all_set:
        if item != last_obj:
            item.delete()


@receiver(pre_save, sender=GlobalSettings)
def update_(sender, instance, **kwargs):
    all_set = GlobalSettings.objects.all()
    last_obj = GlobalSettings.objects.all().first()
    for item in all_set:
        if item != last_obj:
            item.delete()
