from django.contrib.auth.models import AbstractUser
from django.db import models


class AdvUser(AbstractUser):
    t_nicname = models.CharField(max_length=128, null=True, blank=True, verbose_name="Телеграм никнейм")
    chat_id = models.CharField(max_length=128, null=True, blank=True, verbose_name="Телеграм chat_id")

    def __str__(self):
        return f'{self.username}'
