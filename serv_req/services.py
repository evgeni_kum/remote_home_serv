import datetime
import pytz
from django.core.paginator import Paginator
from django.utils import timezone
import calendar

from cabinet.models import AdvUser
from django_site import settings


def calc_adm_pagination(request, obj_list):
    # вычисляем пагинацию
    QUANTITY_OBJ_ON_PAGE = 30  # объектов на странице

    paginator = Paginator(obj_list, QUANTITY_OBJ_ON_PAGE)  # объектов на странице
    if 'page' in request.GET:
        page_num = request.GET['page']
    else:
        page_num = 1
    page = paginator.get_page(page_num)

    # вычисляем переходы до и после текущей
    prev_arr_link = []
    next_arr_link = []
    if page.has_next():
        # если есть след части
        start_num = page.next_page_number()
        end_num = page.paginator.num_pages
        next_arr_link = [x for x in range(start_num, end_num + 1)]

    if page.has_previous():
        start_num = 1
        end_num = page.previous_page_number()
        prev_arr_link = [x for x in range(start_num, end_num + 1)]

    return page, next_arr_link, prev_arr_link, page.object_list


def calc_current_datetime_filter(data):
    current_tz = timezone.get_current_timezone()
    local = data.astimezone(current_tz)
    return local.strftime("%m-%Y")


def calc_max_day(year, month):
    # вычисляем последнее число месяца
    c = calendar.Calendar(0)
    current_calendar = c.monthdayscalendar(year, month)
    # на выходе двумерный список по неделям
    max_day = 0
    for i in current_calendar:
        for y in i:
            if y > max_day:
                max_day = y

    return max_day


def calc_local_date():
    current_timezone = pytz.timezone(settings.TIME_ZONE)
    current_datetime = datetime.datetime.now(tz=current_timezone)  # сохраняем текущее время
    # (year, month, day, weekday, hours, minutes, seconds)
    return current_datetime.strftime("%Y,%m,%d,%w,%H,%M,%S"), current_datetime


def toFixed(numObj, digits=0):
    arr = str(numObj).split('.')
    if len(arr) == 2:
        if len(arr[0]) > 2:
            return f"{numObj:.{0}f}"

    return f"{numObj:.{digits}f}"
