"""django_site URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.urls import re_path
from django.conf.urls.static import static
from django.views.decorators.csrf import csrf_exempt
from django_site import settings
from main.views import index, robots_txt

urlpatterns = [
    path('admin/', admin.site.urls),

    re_path(r'^$', index, name='index'),

    # Cabinet
    path('lk/', include('cabinet.urls')),

    # MainApp
    path('main/', include('main.urls', namespace='main_app')),

    # API
    path('api/v1/', include('api.urls', namespace='api_app')),

    # Serv_Req
    path('s-request/', include('serv_req.urls', namespace='s_request')),

    # Card Index
    path('card-index/', include('card_index.urls', namespace='card_index')),

    # robots.txt
    re_path(r'robots.txt', robots_txt),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)