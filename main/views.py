from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import render
from django.views.decorators.http import require_GET

from django_site import settings
from main.services import get_sensors_data
try:
    from django_site.settings import LINES
except ImportError:
    LINES = [
        "User-Agent: *",
        "Disallow: /"
    ]


def set_additional_data(request):
    """ Указание дополнительных данных"""
    content = {
        "script_version": settings.SCRIPT_VERSION,
    }
    return content


def index(request):
    return render(request, 'index.html')


@login_required(login_url='/lk/login/')
def photo_border(request):
    context = get_sensors_data()
    return render(request, 'photo_border.html', context)


@login_required(login_url='/lk/login/')
def photo_border_ext(request):
    context = get_sensors_data()
    return render(request, 'photo_border_ext.html', context)


@require_GET
def robots_txt(request):
    return HttpResponse("\n".join(LINES), content_type="text/plain")

