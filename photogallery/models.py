from django.db import models
from PIL import Image


class SectionPhoto(models.Model):
    name = models.CharField(max_length=255, verbose_name="Название подраздела услуг")
    rating = models.PositiveIntegerField(verbose_name='Рейтинг', default=100)

    class Meta:
        verbose_name_plural = "Разделы фото галереи"
        verbose_name = "Раздел фото галереи"
        ordering = ['rating']

    def __str__(self):
        """Возвращает строковое представление модели, первые 50 символов"""
        return self.name


_MAX_SIZE = 1920


class Photo(models.Model):
    created = models.DateTimeField(verbose_name='Создано', auto_now_add=True)
    img = models.ImageField(upload_to="photo", verbose_name="Фото",)
    rating = models.PositiveIntegerField(verbose_name='Рейтинг', default=100)
    section = models.ForeignKey(SectionPhoto, on_delete=models.CASCADE)

    def save(self, *args, **kwargs):
        # Сначала - обычное сохранение
        super(Photo, self).save(*args, **kwargs)
        # Проверяем, есть ли изображение?
        if self.img:
            filepath = self.img.path
            width = self.img.width
            height = self.img.height
            max_size = max(width, height)
            # Может, и не надо ничего менять?
            if max_size > _MAX_SIZE:
                # Надо, Федя, надо
                image = Image.open(filepath)
                # resize - безопасная функция, она создаёт новый объект, а не
                # вносит изменения в исходный, поэтому так
                image = image.resize(
                    (round(width / max_size * _MAX_SIZE),  # Сохраняем пропорции
                     round(height / max_size * _MAX_SIZE)),
                    Image.LANCZOS
                )
                # И не забыть сохраниться
                image.save(filepath)

    class Meta:
        verbose_name_plural = "Фото"
        verbose_name = "Фото"
        ordering = ['rating']

    def __str__(self):
        """Возвращает строковое представление модели"""
        return f'{self.created}'
