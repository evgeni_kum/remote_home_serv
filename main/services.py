from django.db.models import Subquery, OuterRef
from serv_req.models import Sensor, Entry
from serv_req.services import calc_local_date


def get_sensors_data():
    sub_query = Subquery(Entry.objects.filter(sensor=OuterRef('pk')).order_by('-created').values('value')[:1])
    all_sensors = Sensor.objects.annotate(last_entry=sub_query).filter(active=True).order_by('rating')
    local_datetime, current_datetime = calc_local_date()  # текущий мес - год,
    # d_list = [i for i in data]  # уходим от объектов Джанго просто к списку
    # d_list.reverse()  # разворачиваем список

    context = {
        'title': 'Главная страница',
        'local_datetime': current_datetime,
        'all_sensors': all_sensors,
    }
    return context