// form search - управление формой поиска
$(document).on("input",function(){
    var q = $("#q-search").val();
    var url = $("#q-search").attr("data-url");
    // console.log(url);
    $.ajax({                       // initialize an AJAX request
        url: url,                    //
        data: {
          'q': q       // add the mark to the GET parameters
        },
        success: function (data) {   // `data` is the return of the `load_cities` view function
            $("#search-frame-result").show();
            $("#search-frame-result").html(data);  // replace the contents of the mark input with the data that came from the server
        }
      });
});
$(document).mouseup(function (e){ // событие клика по веб-документу
    var div = $("#search-frame-result"); // тут указываем ID элемента
    if (!div.is(e.target) // если клик был не по нашему блоку
        && div.has(e.target).length === 0) { // и не по его дочерним элементам
        div.hide(); // скрываем его
    }
});
$(document).on("click", ".item-result", function (){
    var text = $(this).text();
    $('#q-search').val(text);   // передаем текст в поле ввода
    var data_id = $(this).attr("data-id");
    var url_result = $("#q-search").attr("data-result");
    var full_url = url_result + data_id + '/'
    console.log(full_url);
    $("#id-q-search").attr("data-href", full_url);
    $("#search-frame-result").hide();
});
document.getElementById('id-q-search').onclick = function() {
    var q = $("#q-search").val();   // если строка не пустая
    if (q != ''){
        var full_url = $("#id-q-search").attr("data-href");
        if (full_url != ''){
            console.log(full_url);
             $.ajax({                       // initialize an AJAX request
                url: full_url,                    //
                success: function (data) {   //
                    $("#search-str-result").html(data);  //
                }
             });
         } else {
                var url = $("#q-search").attr("data-all-word");
                // console.log(url);
                 $.ajax({                       // initialize an AJAX request
                    url: url,                    //
                    data: {
                      'q': q       //
                    },
                    success: function (data) {   //
                        $("#search-str-result").html(data);
                    }
                 });
         }
    }
};