from django.apps import AppConfig


class ServReqConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'serv_req'
