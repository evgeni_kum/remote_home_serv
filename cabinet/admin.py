from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .forms import CustomUserCreationForm, CustomUserChangeForm
from .models import AdvUser


class CustomUserAdmin(UserAdmin):
    add_form = CustomUserCreationForm
    form = CustomUserChangeForm
    model = AdvUser
    list_display = ('username', 'pk', 'first_name', 'is_staff', 'is_active')
    list_filter = ('username', 'first_name', 'is_staff', 'chat_id',)
    fieldsets = (
        (None, {'fields':
                    ('username', 'password', 'first_name', 'last_name', 'chat_id',
                     't_nicname', )}),
        ('Permissions', {'fields': ('is_staff', 'is_active', 'groups')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('username', 'password1', 'password2', 'is_staff', 'is_active')}
        ),
    )
    search_fields = ('username',)
    ordering = ('username',)


admin.site.register(AdvUser, CustomUserAdmin)

