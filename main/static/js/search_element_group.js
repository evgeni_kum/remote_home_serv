document.getElementById('id-q-search').onclick = function() {
    var q = $("#q-search").val();   // если строка не пустая
    if (q != ''){
        var url = $("#q-search").attr("data-url");
        // console.log(url);
         $.ajax({                       // initialize an AJAX request
            url: url,                    //
            data: {
              'q': q       //
            },
            success: function (data) {   //
                $("#search-str-result").html(data);
            }
         });
    }
};
