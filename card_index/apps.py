from django.apps import AppConfig


class CardIndexConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'card_index'
