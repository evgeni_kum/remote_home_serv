from rest_framework import serializers
from cabinet.models import AdvUser
from photogallery.models import Photo
from serv_req.models import Entry, SetupEntry


class AdvUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = AdvUser
        fields = ('pk', 't_nicname')


class EntrySerializer(serializers.ModelSerializer):
    class Meta:
        model = Entry
        fields = ("sensor", "value", "type")


class PhotoSerializer(serializers.ModelSerializer):

    def to_representation(self, instance):
        ret = super().to_representation(instance)
        ret['class'] = 'photo'
        return ret

    class Meta:
        model = Photo
        fields = ("img", "pk", )



class SetupEntrySerializer(serializers.ModelSerializer):

    class Meta:
        model = SetupEntry
        fields = ("value", "uid", )