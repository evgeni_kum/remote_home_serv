###########
# BUILDER #
###########

FROM python:3.10-alpine

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN apk update \
    && apk add postgresql-dev gcc python3-dev musl-dev tzdata
RUN apk add icu-dev icu-libs icu-data-full

RUN adduser -D app
USER app
WORKDIR /home/app

ENV PATH="/home/app/.local/bin:${PATH}"

RUN pip install --upgrade pip
RUN pip install psycopg2-binary
RUN pip install gunicorn

COPY ./requirements.txt $WORKDIR
RUN pip install -r requirements.txt

COPY ./entrypoint.sh .
COPY --chown=app:app . /home/app

# run entrypoint.sh
ENTRYPOINT ["/home/app/entrypoint.sh"]


#########
# FINAL #
#########