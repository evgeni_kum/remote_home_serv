from django.contrib import admin

from serv_req.models import Sensor, Device, SetupEntry


class AdminSensor(admin.ModelAdmin):
    list_display = ('name', 'uid', 'desc', 'delay', 'rating', 'active', 'internal', 'external', 'avg_value')
    ordering = ('rating',)

    class Media:
        # js = ('js/admin/my_own_admin.js',)
        css = {
            'all': ('css/admin_styles.css',)
        }


admin.site.register(Sensor, AdminSensor)


class SetupEntryAdminInline(admin.TabularInline):
    model = SetupEntry
    extra = 0
    ordering = ('rating',)




class DeviceAdmin(admin.ModelAdmin):
    list_display = ('name', 'uid', 'rating', 'active', )
    ordering = ('name',)
    inlines = [SetupEntryAdminInline]
    readonly_fields = ('uid',)
    search_fields = ('name', 'uid',)

admin.site.register(Device, DeviceAdmin)

