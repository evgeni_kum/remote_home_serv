from django.contrib import admin
from photogallery.models import SectionPhoto, Photo
from django.db import models
from django.contrib.admin.widgets import AdminFileWidget
from django.utils.html import format_html


class CustomAdminFileWidget(AdminFileWidget):
    def render(self, name, value, attrs=None, renderer=None):
        result = []
        if hasattr(value, "url"):
            result.append(
                f'''<a href="{value.url}" target="_blank">
                      <img 
                        src="{value.url}" alt="{value}" 
                        width="150"  style="object-fit: cover;"
                      />
                    </a>'''
            )
        result.append(super().render(name, value, attrs, renderer))
        return format_html("".join(result))


class PhotoInline(admin.TabularInline):
    model = Photo
    extra = 0
    formfield_overrides = {
        models.ImageField: {'widget': CustomAdminFileWidget}
    }


class SectionAdmin(admin.ModelAdmin):
    list_display = ('name', 'rating')
    inlines = [PhotoInline,]


admin.site.register(SectionPhoto, SectionAdmin)



