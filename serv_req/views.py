from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import UpdateView
from cabinet.models import AdvUser


@login_required(login_url='/')
def serv_req(request, filter_result, filter_date, filter_user):

    context = {
        'title': 'Title',

    }
    return render(request, 'serv-req.html', context)


