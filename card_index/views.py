from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from card_index.models import Group, HumanCard


@login_required(login_url='/lk/login/')
def index(request):
    group_slug = request.GET.get('group')
    card_index = None
    if request.user.is_staff:
        groups = Group.objects.all()
    else:
        groups = None
    if group_slug:
        card_index = HumanCard.objects.filter(group__slug=group_slug)
        group = Group.objects.get(slug=group_slug)
        h1 = group.name
        description = 'Картотека '+ group.name
        title = description
    else:
        h1 = 'Картотека'
        description = 'Картотека'
        title = 'Картотека'

    context = {
        'title': title,
        'description': description,
        'groups': groups,
        'cards': card_index,
        'h1': h1,
    }
    return render(request, 'card_index.html', context=context)

