jQuery(document).ready(function($){
    show_data();
    //show_curve_chart();
});

function show_data()
    {
        var url = $("#reload-block").attr("data-reload-url");  // get the url

        $.ajax({
            url: url,
            cache: false,
            success: function (data) {
                $('#reload-block').html(data);
                    setTimeout(function() {
                        show_data();
                    }, 5000);
                },

            error: function (data) {
                $('#messages_task').html("Ошибка!");
            }
        });
    };

function show_curve_chart()
    {
        var url = $("#reload-curve").attr("data-reload-url");  // get the url
        var uid = $("#reload-curve").attr("data-uid");
        $.ajax({
            url: url,
            cache: false,
            data: {
              'uid': uid,       // add the new car id to the GET parameters
            },
            success: function (data) {
                $('#reload-curve').html(data);
                    setTimeout(function() {
                        show_curve_chart();
                    }, 5000);
                },

            error: function (data) {
                $('#messages_task').html("Ошибка!");
            }
        });
    };
