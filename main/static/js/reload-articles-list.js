//===================================================================
const elements = document.querySelectorAll(".work-button");
for (var i = 0; i < elements.length; i++) {
  elements[i].onclick = function(){
     var url = $(this).attr("data-url");

     console.log(url);

     $.ajax({                       // initialize an AJAX request
        url: url,
        cache: false,
        success: function (data) {
            $('#replace-block-articles').html(data);     // выводим данные

        },
        error: function (data) {
            console.log(data);
        }
    });
  };
};