from django.db import models
from django_site.settings import MAX_SIZE
from PIL import Image
from django.contrib import admin


class Group(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField(blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    slug = models.SlugField()

    class Meta:
        ordering = ['name']
        verbose_name_plural = 'Группы'
        verbose_name = 'Группа'

    def __str__(self):
        return self.name

@admin.register(Group)
class AdminGroup(admin.ModelAdmin):
    list_display = ('name', 'created_at',)
    search_fields = ('name',)
    prepopulated_fields = {'slug': ('name',)}


class HumanCard(models.Model):
    name = models.CharField(max_length=200)
    description = models.CharField(max_length=200, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    group = models.ManyToManyField(Group)
    img = models.ImageField(verbose_name='Изображение', upload_to='human_cards', help_text="400х400px", null=True,
                            blank=True)

    class Meta:
        ordering = ['name']
        verbose_name_plural = 'Карточки'
        verbose_name = 'Карточка'

    def __unicode__(self):
        return self.name

    def save(self, *args, **kwargs):
        # Сначала - обычное сохранение
        super(HumanCard, self).save(*args, **kwargs)
        # Проверяем, есть ли изображение?
        if self.img:
            filepath = self.img.path
            width = self.img.width
            height = self.img.height
            max_size = max(width, height)
            # Может, и не надо ничего менять?
            if max_size > MAX_SIZE:
                # Надо, Федя, надо
                image = Image.open(filepath)
                # resize - безопасная функция, она создаёт новый объект, а не
                # вносит изменения в исходный, поэтому так
                image = image.resize(
                    (round(width / max_size * MAX_SIZE),  # Сохраняем пропорции
                     round(height / max_size * MAX_SIZE)),
                    Image.LANCZOS
                )
                # И не забыть сохраниться
                image.save(filepath)


