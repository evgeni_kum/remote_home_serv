from django.contrib import admin
from django.contrib.admin.widgets import AdminFileWidget
from django.utils.html import format_html
from django.db import models
from card_index.models import HumanCard


class CustomAdminFileWidget(AdminFileWidget):
    def render(self, name, value, attrs=None, renderer=None):
        result = []
        if hasattr(value, "url"):
            try:
                result.append(
                    f'''<a href="{value.url}" target="_blank">
                          <img 
                            src="{value.url}" alt="{value}" 
                            width="200" height="200"
                            style="object-fit: cover;"
                          />
                        </a>'''
                )
            except ValueError as e:
                print(e)
        result.append(super().render(name, value, attrs, renderer))
        return format_html("".join(result))


class AdminHumanCard(admin.ModelAdmin):
    list_display = ('name', 'groups_name', 'created_at',)
    search_fields = ('name',)
    autocomplete_fields = ('group',)
    list_filter = ('group',)
    formfield_overrides = {models.ImageField: {'widget': CustomAdminFileWidget}}

    def groups_name(self, obj):
        groups = [i.name for i in obj.group.all()]
        return ", ".join(groups)
    groups_name.short_description = 'Groups'

admin.site.register(HumanCard, AdminHumanCard)
