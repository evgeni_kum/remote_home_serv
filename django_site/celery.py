# from __future__ import absolute_import

import os
from celery import Celery
from celery.schedules import crontab


os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'django_site.settings')

app = Celery('django_site')
app.config_from_object('django.conf:settings')

# время указываем с учетом часового пояса, например нужно чтобы скрипт страбатывал в 15 часов по МСК,
# значит ставим 15 - 3 = 12 часов

app.conf.beat_schedule = {
    'bot_notify_task': {
        'task': 'bot.tasks.set_wait_tasks',
        'schedule': crontab(hour=6, minute=0)   # change to `crontab(minute=0, hour=0)` if you want it to run daily at midnight
        # 'schedule': crontab(minute='*/5'),        # если нужно чтобы скрипт срабатывал каждые 5 минут
    },
}



