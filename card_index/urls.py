from django.urls import path, re_path

from card_index.views import index

app_name = 'c_index'

urlpatterns = [
    re_path(r'^$', index, name='index'),

]
