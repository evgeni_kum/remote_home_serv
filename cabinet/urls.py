from django.urls import path

from cabinet.views import RcLoginView, profile, RcLogoutView

app_name = 'cabinet'

urlpatterns = [
    # url(r'^$', index, name='index'),
    path('login/', RcLoginView.as_view(), name='log_in'),
    path('profile/', profile, name='profile'),
    path('logout/', RcLogoutView.as_view(), name='logout'),
]

