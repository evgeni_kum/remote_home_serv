import uuid
from django.db import models
from django.contrib import admin
from django.utils import timezone

from cabinet.models import AdvUser


class Sensor(models.Model):
    uid = models.UUIDField(
        primary_key=True,
        default=uuid.uuid4,  verbose_name="uniqie device id")
    name = models.CharField(max_length=128, verbose_name="Название")
    desc = models.TextField(blank=True, verbose_name="Описание")
    rating = models.PositiveIntegerField(default=100, verbose_name="Рейтинг")
    delay = models.PositiveIntegerField(default=60, verbose_name="Задержка между данными, минут")
    active = models.BooleanField(default=True, verbose_name='Активность')
    internal = models.BooleanField(default=False, verbose_name='Исп. как температура дом')
    external = models.BooleanField(default=False, verbose_name='Исп. как температура улица')
    avg_value = models.BooleanField(default=False, verbose_name='Вычислять среднее значение')
    iframe = models.CharField(max_length=500, verbose_name="Строка iframe", blank=True)

    class Meta:
        verbose_name_plural = 'Датчики'
        verbose_name = 'Датчик'
        ordering = ['name']

    def __str__(self):
        return f'{self.name}'


class Entry(models.Model):
    # Запись данных
    sensor = models.ForeignKey(Sensor, on_delete=models.CASCADE, verbose_name="Датчик")
    created = models.DateTimeField(default=timezone.now)
    value = models.DecimalField(max_digits=8, decimal_places=2, verbose_name="Значение")
    owner = models.ForeignKey(AdvUser, on_delete=models.SET_NULL, null=True, blank=True,
                              verbose_name='Кто создал запись')
    TYPE_ENTRY = (
        ('temp', "Температура"),
        ('oxygen', "Кислород"),
        ('lyxry', 'Освещение'),
    )
    type = models.CharField(max_length=10, choices=TYPE_ENTRY, default='temp', verbose_name="Тип записи")

    def pyre_dadetime(self):
        current_tz = timezone.get_current_timezone()
        local = self.created.astimezone(current_tz)
        return local.strftime("%H:%M %d/%m/%Y")

    def get_str_val(self):
        return str(self.value)

    class Meta:
        verbose_name_plural = 'Записи с датчиков'
        verbose_name = 'Запись с датчика'
        ordering = ['-created']

    def __str__(self):
        return f'{self.value}'


@admin.register(Entry)
class AdminEntry(admin.ModelAdmin):
    list_display = ('sensor', 'created', 'value', 'owner', 'type')
    # search_fields = ('type',)

# ---------------------------------------------------------


class EntryInBuffer(models.Model):
    # Запись данных в буффере
    sensor = models.ForeignKey(Sensor, on_delete=models.CASCADE, verbose_name="Датчик")
    value = models.DecimalField(max_digits=10, decimal_places=2, verbose_name="Значение")
    count_value = models.PositiveIntegerField(default=0, verbose_name="Номер в буффере")
    created = models.DateTimeField(default=timezone.now)

    class Meta:
        verbose_name_plural = 'Записи данных в буффере'
        verbose_name = 'Запись данных в буффере'
        ordering = ['-created']

    def __str__(self):
        return f'{self.value}'


@admin.register(EntryInBuffer)
class AdminEntryInBuffer(admin.ModelAdmin):
    list_display = ('sensor', 'created', 'value', 'count_value')


class Device(models.Model):
    uid = models.UUIDField(
        primary_key=True,
        default=uuid.uuid4,  verbose_name="uniqie device id")
    name = models.CharField(max_length=128, verbose_name="Название")
    desc = models.TextField(blank=True, verbose_name="Описание")
    rating = models.PositiveIntegerField(default=100, verbose_name="Рейтинг")
    active = models.BooleanField(default=True, verbose_name='Активность')

    class Meta:
        verbose_name_plural = 'Устройства'
        verbose_name = 'Устройство'
        ordering = ['name']

    def __str__(self):
        return f'{self.name}'


class SetupEntry(models.Model):
    device = models.ForeignKey(Device, on_delete=models.CASCADE)
    value = models.PositiveIntegerField(verbose_name="Значение", default=0)
    uid = models.CharField(unique=True, max_length=128, verbose_name="Уникальный id")
    desc = models.TextField(blank=True, verbose_name="Описание")
    rating = models.PositiveIntegerField(default=100, verbose_name="Рейтинг")

class Meta:
    verbose_name_plural = 'Настройки'
    verbose_name = 'Настройка'
    ordering = ['device']
    unique_together = ('device', 'uid')

def __str__(self):
    return f'{self.device}'

"""
class Job(models.Model):
    name = models.CharField(max_length=128, verbose_name="Название")
    device = models.ForeignKey(Device, on_delete=models.SET_NULL, null=True, blank=True, verbose_name="Устройство")
"""

