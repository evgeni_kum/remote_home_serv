from django.urls import path
from api.views import EntryView, EntryArrayView, EntryTestView, entries_data, get_photo, settings_data

app_name = 'api'

urlpatterns = [
    #
    path('write-data/', EntryView.as_view()),                       # POST
    path('write-array-data/', EntryArrayView.as_view()),            # POST
    path('write-client-data/', EntryTestView.as_view()),                       # POST
    path('entries-data/', entries_data, name="entries_data"),         # GET

    path('settings-data/', settings_data, name="settings_data"),         # GET

    path('photo/', get_photo, name="get_photo"),
]

